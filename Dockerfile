FROM alpine

# Update OS
RUN apk upgrade --no-cache

# Install runtime dependencies
RUN apk add --no-cache \
  ca-certificates \
  gettext \
  libjpeg \
  libpq \
  libxslt \
  nginx \
  python3 \
  wget

# Collect the taiga-back version to download from github as a build argument
# and save it in an environment variable
ARG TAIGA_BACK_VERSION
ENV TAIGA_BACK_VERSION=$TAIGA_BACK_VERSION

# Download and install the taiga-front-dist from the github release
RUN \
  mkdir -p /usr/share/taiga-back/ && \
  cd /usr/share/taiga-back/ && \
  wget https://github.com/taigaio/taiga-back/archive/${TAIGA_BACK_VERSION}.tar.gz && \
  tar -xzf ${TAIGA_BACK_VERSION}.tar.gz && \
  rm ${TAIGA_BACK_VERSION}.tar.gz && \
  mv taiga-back-${TAIGA_BACK_VERSION}/* . && \
  rm -r taiga-back-${TAIGA_BACK_VERSION} && \
  chown -R nginx .

# Run our Taiga back install script to install it in one Docker commit.
# This way we can save space by uninstalling unneeded packages when we are
# done with them. 
COPY install-taiga-back.sh /install-taiga-back.sh
RUN \
  chmod 744 /install-taiga-back.sh && \
  /install-taiga-back.sh

# Install Gunicorn cli
RUN apk add --no-cache py-gunicorn
# Symlink python3 to python so that the Gunicorn cli will work with it
RUN ln -s /usr/bin/python3 /usr/bin/python

# Create required nginx run directory
RUN mkdir -p /run/nginx

# Remove default nginx config
RUN rm /etc/nginx/conf.d/default.conf
RUN rm /etc/nginx/nginx.conf

# Copy in our nginx config
COPY taiga-back.conf /etc/nginx/conf.d/taiga-back.conf
COPY nginx.conf /etc/nginx/nginx.conf

# Make the Taiga media and static folders
RUN mkdir -p /usr/share/taiga-back/media
RUN chown nginx /usr/share/taiga-back/media
RUN mkdir -p /usr/share/taiga-back/static
RUN chown nginx /usr/share/taiga-back/static

# Install static data
RUN \
  su -s /bin/sh -c 'python3 /usr/share/taiga-back/manage.py compilemessages' nginx && \
  su -s /bin/sh -c 'python3 /usr/share/taiga-back/manage.py collectstatic --noinput' nginx

# Copy in our Taiga configuration template
COPY local.py /usr/share/taiga-back/settings/local.py

# Copy in the Taiga configuration script
COPY configure-taiga.sh /configure-taiga.sh
RUN chmod 744 /configure-taiga.sh

# Copy in our Taiga start script
COPY start-taiga.sh /usr/share/taiga-back/start-taiga.sh
RUN chmod 744 /usr/share/taiga-back/start-taiga.sh
RUN chown nginx /usr/share/taiga-back/start-taiga.sh

# Copy in the container-start and container-stop scripts
COPY start-container.sh /start-container.sh
RUN chmod 744 /start-container.sh

COPY stop-container.sh /stop-container.sh
RUN chmod 744 /stop-container.sh

# Copy in our docker-cmd script
COPY docker-cmd.sh /docker-cmd.sh
RUN chmod 744 /docker-cmd.sh

CMD ["/docker-cmd.sh"]
