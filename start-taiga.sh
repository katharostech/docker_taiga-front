cd /usr/share/taiga-back/ 

python manage.py migrate --noinput
python manage.py loaddata initial_user
python manage.py loaddata initial_project_templates
#python manage.py compilemessages
#python manage.py collectstatic --noinput

gunicorn --daemon --pid /var/run/gunicorn.pid --user nginx --group nginx \
  --workers 4 --timeout 60 --pythonpath=. --bind 127.0.0.1:8001 taiga.wsgi
