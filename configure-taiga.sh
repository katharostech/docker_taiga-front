#!/bin/sh

settings_file=/usr/share/taiga-back/settings/local.py

# Set default values
DB_NAME=${DB_NAME-taiga}
DB_USER=${DB_USER-taiga}


# Subsitute the variables in the settings file template
sed -i s/\${DB_NAME}/${DB_NAME}/ $settings_file
sed -i s/\${DB_USER}/${DB_USER}/ $settings_file
sed -i s/\${DB_PASSWORD}/${DB_PASSWORD}/ $settings_file
sed -i s/\${DB_HOST}/${DB_HOST}/ $settings_file
sed -i s/\${DB_PORT}/${DB_PORT}/ $settings_file
sed -i s/\${SITE_PROTO}/${SITE_PROTO}/ $settings_file
sed -i s/\${SITE_DOMAIN}/${SITE_DOMAIN}/ $settings_file
sed -i s/\${SITE_PORT}/${SITE_PORT}/ $settings_file
