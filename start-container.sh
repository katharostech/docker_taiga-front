#!/bin/sh

# Configure taiga with the passed-in environment variables
/configure-taiga.sh

# Start Taiga back running on Gunicorn
/usr/share/taiga-back/start-taiga.sh

# Start nginx
nginx
