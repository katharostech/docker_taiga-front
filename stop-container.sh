#!/bin/sh

# Stop nginx
nginx -s stop

# Stop gunicorn
kill -SIGTERM $(cat /var/run/gunicorn.pid)
