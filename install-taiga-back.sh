# Install 3rd party library build time dependencies
apk add --no-cache \
  g++ \
  git \
  jpeg-dev \
  libffi-dev \
  libxslt-dev \
  postgresql-dev \
  py3-cffi \
  python3-dev \
  zlib-dev

# Install Python package dependencies
pip3 install --upgrade pip
pip3 install -r /usr/share/taiga-back/requirements.txt

# Taiga seems to want write access to django
chown -R nginx /usr/lib/python3.5/site-packages/django

# Remove no longer needed build dependencies
apk del \
  g++ \
  git \
  jpeg-dev \
  libffi-dev \
  libxslt-dev \
  postgresql-dev \
  py3-cffi \
  python3-dev \
  zlib-dev
